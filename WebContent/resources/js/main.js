$(function(){
	$(".project").click(function(e) {
		var idProject = $(this).find(".project_id").val();
		
		window.location.href = "project?idProject=" + idProject;
	});
	
	$(".project_task_checkbox").change(function(e) {
		e.preventDefault();
		
		var check = $(this);
		var data = {
			"task.id" : check.parent().find(".project_task_id").val(),
			"task.finished" : check.is(":checked")
		};
		
		$.post("updateFinishedTask", data, function() {
			window.location.reload();
		});
	});
	
	/*$("#task_input_form").submit(function(e) {
		var descriptionInput = $("#task_input_description");
		var description = descriptionInput.val();
		var success = true;
		
		descriptionInput.css("border", "");
		
		if (description.length >= 512 || description.length < 3) {
			descriptionInput.css("border", "solid 1px red");
			success = false;
		}
		
		if (!success) {
			e.preventDefault();
		} else {
			$(this).submit();
		}
	});*/
});