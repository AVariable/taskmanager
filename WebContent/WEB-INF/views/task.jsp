<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="task_container">
	<div id="task_header">
		<div id="task_header_left">
			<h1 id="task_project_title">${ project.title }</h1>
		</div>
		<div id="task_header_right">
			<span>test</span>
		</div>
	</div>
	<div id="task_information">
		<p id="task_description">${ task.description }</p>
		<p class="task_date">Criado em: <fmt:formatDate value="${ task.dateCreated.time }" pattern="dd/MM/yyyy" /></p>
		<s:if test="task.finished">
			<p class="task_date">Finalizado a: <fmt:formatDate value="${ task.dateFinished.time }" pattern="dd/MM/yyyy" /></p>
		</s:if>
		
		<div id="task_users">
			<h3>Responsáveis</h3>
			<s:iterator value="task.users" var="user">
				<div class="task_user">
					<p>${ user.name }</p>
				</div>
			</s:iterator>
		</div>
	</div>
</div>