<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div>
	<div id="header_nav">
		<s:if test="%{ #session.idUser != null }">
			<ul>
				<li><a href="home">Home</a></li>
				<li><a href="logout">Logout</a></li>
			</ul>
		</s:if>
	</div>
	<div id="header_description">Task Manager</div>
	<div id="header_logo"></div>
</div>