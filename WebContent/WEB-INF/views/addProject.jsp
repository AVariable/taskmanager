<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<form id="project_add_form" class="default_form" action="insertProject" method="post">
	<div>
		<label for="title">Título</label>
		<div>
			<input type="text" name="project.title" />
		</div>
	</div>
	
	<div>
		<label for="description">Descrição</label>
		<div>
			<textarea id="task_input_description" name="project.description"></textarea>
		</div>
	</div>
	<input type="submit" value="Adicionar" />		
</form>