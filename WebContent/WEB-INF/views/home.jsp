<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="menu_buttons">
	<div class="button">
		<a href="addProject">Novo Projecto</a>
	</div>
</div>
<div id="projects">
	<s:iterator value="projects" var="project">
		<div class="project">
			<input type="hidden" class="project_id" value="${ project.id }" />
			<h4 class="project_title">${ project.title }</h4>
			<p class="project_description">${ project.description }</p>
			<p class="project_updated">Actualizado em: <fmt:formatDate value="${ project.dateUpdated.time }" pattern="dd/MM/yyyy" /></p>
		</div>
	</s:iterator>
</div>