<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="project_header">
	<h1>${ project.title }</h1>
	<p>${ project.description }</p>
</div>

<hr>

<div id="project_tasks">
	<div>
		<s:iterator value="unfinishedTasks" var="task">
			<div class="project_task">
				<input type="hidden" class="project_task_id" value="${ task.id }" />
				<input type="checkbox" class="project_task_checkbox" />
				<a href="task?task.id=${ task.id }" class="project_task_description">${ task.description }</a>
			</div>
		</s:iterator>
	</div>

	<div id="project_tasks_finished">
		<s:iterator value="finishedTasks" var="task">
			<div class="project_task">
				<input type="hidden" class="project_task_id" value="${ task.id }" />
				<input type="checkbox" class="project_task_checkbox" checked />
				<a href="task?task.id=${ task.id }" class="project_task_description">${ task.description }</a>
			</div>
		</s:iterator>
	</div>
</div>