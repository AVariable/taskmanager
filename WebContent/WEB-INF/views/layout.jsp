<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<link rel="shortcut icon" href="http://www.primeit.pt/public/imgs/id/favicon.ico">
<link rel="icon" sizes="192x192" href="http://www.primeit.pt/public/imgs/id/favicon.png">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic|Montserrat:400,700|PT+Serif:400,400italic">
<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/resources/css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="${ pageContext.request.contextPath }/resources/js/main.js"></script>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>
		
		<div id="content">
			<div id="container">
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		
		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>