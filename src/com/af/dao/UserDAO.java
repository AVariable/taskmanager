package com.af.dao;

import com.af.model.User;

public interface UserDAO {

	public User getUser(int id);
	
	public User getUserByAuth(String email, String password);
	
	public void updateUser(User user);
}
