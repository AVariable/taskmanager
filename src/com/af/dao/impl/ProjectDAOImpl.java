package com.af.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.af.dao.ProjectDAO;
import com.af.model.Project;

@Repository
public class ProjectDAOImpl implements ProjectDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Project getProject(int id) {
		return em.find(Project.class, id);
	}
	
	@Override
	public Project getProjectByIdUser(int idProject, int idUser) {
		TypedQuery<Project> query = em.createNamedQuery("getProjectByIdUser", Project.class);
		List<Project> projects = null;
		
		query.setParameter("idProject", idProject);
		query.setParameter("idUser", idUser);
		projects = query.getResultList();
		
		if (!projects.isEmpty()) {
			return projects.get(0);
		}
		
		return null;
	}

	@Override
	public List<Project> getProjectsByIdUser(int idUser) {
		TypedQuery<Project> query = em.createNamedQuery("getProjectsByIdUser", Project.class);
		
		query.setParameter("idUser", idUser);
		
		return query.getResultList();
	}
	
	@Override
	public void insertProject(Project project) {
		em.persist(project);
	}
}
