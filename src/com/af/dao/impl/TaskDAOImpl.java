package com.af.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.af.dao.TaskDAO;
import com.af.model.Task;

@Repository
public class TaskDAOImpl implements TaskDAO {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Task getTask(int id) {
		TypedQuery<Task> query = em.createNamedQuery("getTask", Task.class);
		List<Task> tasks = null;
		
		query.setParameter("idTask", id);
		tasks = query.getResultList();
		
		if (!tasks.isEmpty()) {
			return tasks.get(0);
		}
		
		return null;
	}
	
	@Override
	public void updateTask(Task task) {
		em.merge(task);
	}
}
