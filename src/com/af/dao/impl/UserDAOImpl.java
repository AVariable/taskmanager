package com.af.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.af.dao.UserDAO;
import com.af.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public User getUser(int id) {
		TypedQuery<User> query = em.createNamedQuery("getUser", User.class);
		List<User> users = null;
		
		query.setParameter("idUser", id);
		users = query.getResultList();
		
		if (!users.isEmpty())
			return users.get(0);
		return null;
	}
	
	@Override
	public User getUserByAuth(String email, String password) {
		TypedQuery<User> query = em.createNamedQuery("getUserByAuth", User.class);
		List<User> users = null;
		
		query.setParameter("email", email);
		query.setParameter("password", password);
		users = query.getResultList();
		
		if (!users.isEmpty())
			return users.get(0);
		return null;
	}
	
	@Override
	public void updateUser(User user) {
		em.merge(user);
	}
}
