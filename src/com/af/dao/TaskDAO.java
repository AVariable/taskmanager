package com.af.dao;

import com.af.model.Task;

public interface TaskDAO {

	public Task getTask(int id);
	
	public void updateTask(Task task);
}
