package com.af.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "user")
@NamedQueries({
	@NamedQuery(name = "getUserByAuth", query = "from User where email=:email and password=:password"),
	@NamedQuery(name = "getUser", query = "select u from User u join fetch u.projects where u.id=:idUser")
})
public class User {

	private int id;
	private String email;
	private String password;
	private String name;
	private Set<Project> projects;
	private Set<Task> tasks;

	public User() {
		projects = new HashSet<Project>(0);
		tasks = new HashSet<Task>(0);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public int getId() {
		return id;
	}

	@Column(name = "email", length = 255, nullable = false)
	public String getEmail() {
		return email;
	}

	@Column(name = "password", length = 45, nullable = false)
	public String getPassword() {
		return password;
	}

	@Column(name = "name", length = 255, nullable = true)
	public String getName() {
		return name;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "user_project", joinColumns = {
			@JoinColumn(name = "id_user", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_project", nullable = false, updatable = false) })
	public Set<Project> getProjects() {
		return projects;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "user_task", joinColumns = {
			@JoinColumn(name = "id_user", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_task", nullable = false, updatable = false) })
	public Set<Task> getTasks() {
		return tasks;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

}
