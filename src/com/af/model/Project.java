package com.af.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
@NamedQueries({
	@NamedQuery(name = "getProjectByIdUser", query = "select p from Project p inner join p.users User left join fetch p.tasks where User.id=:idUser AND p.id=:idProject"),
	@NamedQuery(name = "getProjectsByIdUser", query = "select p from Project p inner join p.users User where User.id=:idUser")
})
public class Project {

	private int id;
	private String title;
	private String description;
	private Calendar dateCreated;
	private Calendar dateUpdated;
	private Set<User> users;
	private Set<Task> tasks;

	public Project() {
		users = new HashSet<User>(0);
		tasks = new HashSet<Task>(0);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public int getId() {
		return id;
	}

	@Column(name = "title", length = 45, nullable = false)
	public String getTitle() {
		return title;
	}

	@Column(name = "description", length = 255, nullable = true)
	public String getDescription() {
		return description;
	}

	@Column(name = "date_created", nullable = true)
	public Calendar getDateCreated() {
		return dateCreated;
	}

	@Column(name = "date_updated", nullable = true)
	public Calendar getDateUpdated() {
		return dateUpdated;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "projects")
	public Set<User> getUsers() {
		return users;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
	public Set<Task> getTasks() {
		return tasks;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDateCreated(Calendar dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDateUpdated(Calendar dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public void setUsers(Set<User> projectUsers) {
		this.users = projectUsers;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

}
