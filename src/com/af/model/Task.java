package com.af.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "task")
@NamedQueries({
	@NamedQuery(name = "getTask", query = "select t from Task t join fetch t.users where t.id=:idTask")
})
public class Task {

	private int id;
	private String description;
	private boolean finished;
	private Calendar dateCreated;
	private Calendar dateFinished;
	private Project project;
	private Set<User> users;

	public Task() {
		users = new HashSet<User>(0);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public int getId() {
		return id;
	}

	@Column(name = "description", length = 512, nullable = false)
	public String getDescription() {
		return description;
	}

	@Column(name = "finished", nullable = false)
	public boolean isFinished() {
		return finished;
	}

	@Column(name = "date_created", nullable = true)
	public Calendar getDateCreated() {
		return dateCreated;
	}

	@Column(name = "date_finished", nullable = true)
	public Calendar getDateFinished() {
		return dateFinished;
	}

	@ManyToOne
	@JoinColumn(name = "id_project", nullable = false)
	public Project getProject() {
		return project;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "tasks", cascade = CascadeType.ALL)
	public Set<User> getUsers() {
		return users;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public void setDateCreated(Calendar dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setDateFinished(Calendar dateFinished) {
		this.dateFinished = dateFinished;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

}
