package com.af.service;

import com.af.model.Task;

public interface TaskService {

	public Task getTask(int id);
	
	public void updateTask(Task task);
}
