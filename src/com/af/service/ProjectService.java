package com.af.service;

import java.util.List;

import com.af.model.Project;

public interface ProjectService {

public Project getProject(int id);
	
	public Project getProjectByIdUser(int idProject, int idUser);
	
	public List<Project> getProjectsByIdUser(int idUser);
	
	public void insertProject(Project project);
}
