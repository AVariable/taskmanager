package com.af.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.af.dao.ProjectDAO;
import com.af.model.Project;
import com.af.service.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectDAO projectDAO;
	
	@Override
	@Transactional
	public Project getProject(int id) {
		return projectDAO.getProject(id);
	}

	@Override
	@Transactional
	public Project getProjectByIdUser(int idProject, int idUser) {
		Project project = projectDAO.getProjectByIdUser(idProject, idUser);
				
		return project;
	}

	@Override
	@Transactional
	public List<Project> getProjectsByIdUser(int idUser) {
		return projectDAO.getProjectsByIdUser(idUser);
	}

	@Override
	@Transactional
	public void insertProject(Project project) {
		projectDAO.insertProject(project);
	}

}
