package com.af.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.af.dao.TaskDAO;
import com.af.model.Task;
import com.af.service.TaskService;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskDAO taskDAO;
	
	@Override
	@Transactional
	public Task getTask(int id) {
		Task task = taskDAO.getTask(id);
		
		return task;
	}
	
	@Override
	@Transactional
	public void updateTask(Task task) {
		taskDAO.updateTask(task);
	}

}
