package com.af.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.af.dao.UserDAO;
import com.af.model.User;
import com.af.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	@Transactional
	public User getUser(int id) {
		User user = userDAO.getUser(id);
		
		if (user != null) {
			user.getProjects().size();
		}
		
		return user;
	}

	@Override
	@Transactional
	public User getUserByAuth(String email, String password) {
		return userDAO.getUserByAuth(email, password);
	}

	@Override
	@Transactional
	public void updateUser(User user) {
		userDAO.updateUser(user);
	}

}
