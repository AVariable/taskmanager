package com.af.service;

import com.af.model.User;

public interface UserService {

	public User getUser(int id);

	public User getUserByAuth(String email, String password);

	public void updateUser(User user);
}
