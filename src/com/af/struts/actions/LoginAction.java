package com.af.struts.actions;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.af.model.User;
import com.af.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = -6986107449393098442L;

	@Autowired
	private UserService userService;
	
	private String email;
	private String password;
	private Map<String, Object> session;

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public String execute() throws Exception {
		return super.execute();
	}

	public String logout() {
		session.clear();
		
		return SUCCESS;
	}
	
	public String verify() {
		User user = userService.getUserByAuth(email, password);
		
		if (user != null) {
			session.put("idUser", new Integer(user.getId()));
			
			return SUCCESS;
		}
		
		return ERROR;
	}
}
