package com.af.struts.actions;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;

import com.af.model.Project;
import com.af.model.Task;
import com.af.service.ProjectService;
import com.af.service.TaskService;
import com.opensymphony.xwork2.ActionSupport;

public class TaskAction extends ActionSupport {

	private static final long serialVersionUID = -7361310080528920965L;
	
	@Autowired
	private ProjectService projectService;
	@Autowired
	private TaskService taskService;
	
	private Project project;
	private Task task;
	
	public Project getProject() {
		return project;
	}
	
	public Task getTask() {
		return task;
	}
	
	public void setProject(Project project) {
		this.project = project;
	}
	
	public void setTask(Task task) {
		this.task = task;
	}
	
	@Override
	public String execute() throws Exception {
		task = taskService.getTask(task.getId());
		project = projectService.getProject(task.getProject().getId());
		
		return super.execute();
	}
	
	public String updateFinishedTask() {
		Task task = taskService.getTask(this.task.getId());
		
		task.setFinished(this.task.isFinished());
		if (task.isFinished()) {
			task.setDateFinished(Calendar.getInstance());
		}
		taskService.updateTask(task);
		
		return SUCCESS;
	}
	
}
