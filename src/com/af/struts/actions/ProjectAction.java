package com.af.struts.actions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.af.model.Project;
import com.af.model.Task;
import com.af.model.User;
import com.af.service.ProjectService;
import com.af.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

public class ProjectAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = -4196434612616213433L;

	@Autowired
	private ProjectService projectService;
	@Autowired
	private UserService userService;
	
	private Map<String, Object> session;
	private int idProject;
	private Project project;
	private List<Task> unfinishedTasks;
	private List<Task> finishedTasks;

	public ProjectAction() {
		unfinishedTasks = new ArrayList<Task>();
		finishedTasks = new ArrayList<Task>();
	}
	
	public Map<String, Object> getSession() {
		return session;
	}

	public int getIdProject() {
		return idProject;
	}
	
	public Project getProject() {
		return project;
	}
	
	public List<Task> getUnfinishedTasks() {
		return unfinishedTasks;
	}
	
	public List<Task> getFinishedTasks() {
		return finishedTasks;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	
	public void setProject(Project project) {
		this.project = project;
	}
	
	public void setUnfinishedTasks(List<Task> unfinishedTasks) {
		this.unfinishedTasks = unfinishedTasks;
	}
	
	public void setFinishedTasks(List<Task> finishedTasks) {
		this.finishedTasks = finishedTasks;
	}
	
	@Override
	public String execute() throws Exception {
		int idUser = (int)session.get("idUser");
		
		project = projectService.getProjectByIdUser(idProject, idUser);
		
		for (Task task : project.getTasks()) {
			if (!task.isFinished()) {
				unfinishedTasks.add(task);
			} else {
				finishedTasks.add(task);
			}
		}
		
		return project != null ? SUCCESS : ERROR;
	}
	
	public String addProject() {
		return SUCCESS;
	}
	
	public String insertProject() {
		int idUser = (int)session.get("idUser");
		User user = userService.getUser(idUser);
		
		user.getProjects().add(project);
		project.setDateCreated(Calendar.getInstance());
		project.setDateUpdated(Calendar.getInstance());
		projectService.insertProject(project);
		userService.updateUser(user);
		
		return SUCCESS;
	}
	
}
