package com.af.struts.actions;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.af.model.Project;
import com.af.service.ProjectService;
import com.opensymphony.xwork2.ActionSupport;

public class HomeAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = -6986107449393098442L;

	@Autowired
	private ProjectService projectService;
	
	private Map<String, Object> session;
	private List<Project> projects;

	public Map<String, Object> getSession() {
		return session;
	}
	
	public List<Project> getProjects() {
		return projects;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	@Override
	public String execute() throws Exception {
		int idUser = (int)session.get("idUser");
		
		projects = projectService.getProjectsByIdUser(idUser);
		
		return super.execute();
	}
}
