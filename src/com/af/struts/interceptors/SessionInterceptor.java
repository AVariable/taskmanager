package com.af.struts.interceptors;

import java.util.Map;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class SessionInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -2280123401057205266L;

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> session = invocation.getInvocationContext().getSession();
		Integer idUser = (Integer)session.get("idUser");
		
		if (idUser == null) {
			return Action.LOGIN;
		}
		
		return invocation.invoke();
	}

}
